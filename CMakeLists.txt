cmake_minimum_required(VERSION 3.6)
project(GameOfLife)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Werror -pedantic -ansi -g -std=c11")

set(SOURCE_FILES main.c config.c game.c)
add_executable(GameOfLife ${SOURCE_FILES})