

#include "game.h"

void game_debug(Game* game, int stage)

{

    printf("DEBUG stage: %d \n", stage);

    game_print_board(game);

    printf("\n\n");

}

int main(int argc, char*argv[])

{

    GameConfig *config;

    Game *game = NULL;

    game = game_new();

    config = game_config_new_from_cli(argc,argv);

    if( game_parse_board(game,config) == 0)

    {

        if(config->debug)

        {

            int i;

            for (i = 0; i <= config->generations; ++i) {

                game_debug(game,i);

                game_tick(game);

            }

        }

        else if(config->silent)

        {

            int i;

            for (i = 0; i <= config->generations; ++i) {

                game_tick(game);

            }

        }

        else

        {

            game_print_board(game);

            int i;

            for (i= 0; i <= config->generations; ++i) {

                game_tick(game);

            }

            printf("\n");

            game_print_board(game);

        }

    } else

    {

        perror("Error when parsing board");

    }





    game_free(game);

    game_config_free(config);

    return  0;

}