CC=gcc-5
CFLAGS=-Wall -g -ansi -pedantic -fcilkplus -std=c11
LDFLAGS=

OUTPUT=glife
SRC=config.c game.c main.c
OBJ=$(patsubst %.c,%.o,$(SRC))


%.o: %.c
	$(CC) $(CFLAGS) $<

all: $(OUTPUT)
	$(CC) $(CFLAGS) $(SRC) $(LDFLAGS) -o $(OUTPUT)


clean:
	rm -f $(OUTPUT) $(OBJ)
