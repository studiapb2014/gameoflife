#include <ctype.h>
#include "config.h"
#include <getopt.h>
#include <string.h>


void game_config_free(GameConfig *config)
{
	free(config);
}
size_t game_config_get_generations(GameConfig *config)
{
    return  config->generations;
}

GameConfig *game_config_new_from_cli(int argc, char *argv[])
{
    GameConfig *config;
    config = (GameConfig *) malloc(sizeof( GameConfig));
    int c;
    config->generations = 20;
    config->silent =0;
    config->debug =0;

    const char    * short_opt = "dsn:";
    /*struct option   long_opt[] =
            {
                    // {"help",          no_argument,       NULL, 'h'},
                    //{"file",          required_argument, NULL, 'f'},
                    {NULL,            0,                 NULL, 0  }
            };

*/
    while((c = getopt_long(argc, argv, short_opt, NULL, NULL)) != -1) {
   /* while((c = getopt(argc, argv, short_opt)) != -1) {*/
        switch (c) {
            case 'd':
                config->debug = 1;
                config->silent = 0;
                break;
            case 's':
                config->silent = 1;
                config->debug = 0;
                break;
            case 'n':
               config->generations =  (size_t )atoi(optarg);
                break;
            default:break;
        }
    }

    int index;
    for (index = optind; index < argc; index++)
    {
        FILE *fp;
        char* dir = "tests/";
        char* newdir = (char*) malloc(strlen(dir) + strlen(argv[index]));
        strcpy(newdir,dir);
        strcat(newdir,argv[index]);
        fp=fopen(newdir, "r");

        if(fp == NULL)
        {
            perror("Error while opening file");
        }
        config->input_file = fp;
    }



    return config;
}